import { getRandomColor, getSortStateByKey } from './utils.transfer';

describe('Utils testing', () => {

  it(`should getRandomColor have return typeof string`, () => {
    expect(getRandomColor()).toHaveBeenCalled();
    expect(getRandomColor()).toEqual(jasmine.any(String));
  });

  it(`should return DATE_ASC`, () => {
    expect(getSortStateByKey('DATE', 'DATE_DESC')).toHaveBeenCalled();
    expect(getSortStateByKey('DATE', 'DATE_DESC')).toEqual('DATE_ASC');
  });
  it(`should return DATE_DESC`, () => {
    expect(getSortStateByKey('DATE', 'DATE_ASC')).toEqual('DATE_DESC');
  });
});

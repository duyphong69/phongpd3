import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IRecentTransaction } from '../shared/transfer.models';
import { TRANSFER_STATE } from './transfer.const';
@Injectable({
  providedIn: 'root'
})
export class TransferService {

  recentTransactions$ = new BehaviorSubject<IRecentTransaction[]>(null);
  sortState$ = new BehaviorSubject<string>(null);
  transferState$ = new BehaviorSubject<TRANSFER_STATE>(null);
  amount = 5824.76;
  constructor() { }
}

import { IRecentTransaction } from './transfer.models';
import { DATAS_MAP_IMG, SORT_STATE } from './transfer.const';

export function findImgPath(ecentTransaction: IRecentTransaction): string {
  let result = 'assets/images/icons/backbase.png'; // default path
  const resultFind = DATAS_MAP_IMG.find(data => data.name === ecentTransaction.merchant.name);
  if (resultFind) {
    result = resultFind.pathImg;
  }
  return result;
}

export function getSortStateByKey(key, curSortState): string {
  let result;
  const keyASC = `${key}_ASC`;
  const keyDESC = `${key}_DESC`;
  if (curSortState.includes(key)) {
    result = (SORT_STATE[keyASC] === curSortState) ? SORT_STATE[keyDESC] : SORT_STATE[keyASC];
  } else {
    result = curSortState.includes('ASC') ? SORT_STATE[keyASC] : SORT_STATE[keyDESC];
  }
  return result;
}

export function getRandomColor(): string {
  const months = ['#fbbb1b', '#12a580', '#1180aa', '#e25a2c', '#c89616', '#c12020', '#d51271'];
  const randomColor = Math.floor(Math.random() * months.length);
  return months[randomColor];
}

export const SORT_STATE = {
  DATE_ASC: 'DATE_ASC',
  DATE_DESC: 'DATE_DESC',
  BENEFICARY_ASC: 'BENEFICARY_ASC',
  BENEFICARY_DESC: 'BENEFICARY_DESC',
  AMOUNT_ASC: 'AMOUNT_ASC',
  AMOUNT_DESC: 'AMOUNT_DESC'
};

export enum TRANSFER_STATE {
  INPUT,
  CONFIRM,
  SUCCESSFULLY,
  FAILED
}

export const DATAS_MAP_IMG = [
  {
    name: 'Backbase',
    pathImg: 'assets/images/icons/backbase.png'
  },
  {
    name: 'The Tea Lounge',
    pathImg: 'assets/images/icons/the-tea-lounge.png'
  },
  {
    name: 'Texaco',
    pathImg: 'assets/images/icons/texaco.png'
  },
  {
    name: 'The Tea Lounge',
    pathImg: 'assets/images/icons/the-tea-lounge.png'
  },
  {
    name: 'Amazon Online Store',
    pathImg: 'assets/images/icons/amazon-online-store.png'
  },
  {
    name: '7-Eleven',
    pathImg: 'assets/images/icons/7-eleven.png'
  },
  {
    name: 'H&M Online Store',
    pathImg: 'assets/images/icons/h&m-online-store.png'
  },
  {
    name: 'Jerry Hildreth',
    pathImg: 'assets/images/icons/jerry-hildreth.png'
  },
  {
    name: 'Lawrence Pearson',
    pathImg: 'assets/images/icons/lawrence-pearson.png'
  },
  {
    name: 'Whole Foods',
    pathImg: 'assets/images/icons/whole-foods.png'
  },
  {
    name: 'Southern Electric Company',
    pathImg: 'assets/images/icons/southern-electric-company.png'
  }
];

export const regexCheckDecimal = /^(\d+\.?\d{0,9}|\.\d{1,2})$/; // Limit to 2 decimal places

export const MONTH = {
  '01': 'Jan',
  '02': 'Feb',
  '03': 'Mar',
  '04': 'Apr',
  '05': 'May',
  '06': 'Jun',
  '07': 'Jul',
  '08': 'Aug',
  '09': 'Sep',
  10: 'Oct',
  11: 'Nov',
  12: 'Dec',
};

export const KEY_SORT = {
  DATE: 'DATE',
  BENEFICARY: 'BENEFICARY',
  AMOUNT: 'AMOUNT'
};

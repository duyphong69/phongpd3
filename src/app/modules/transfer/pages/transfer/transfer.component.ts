import { Component, OnInit } from '@angular/core';
import { TransferService } from '../../shared/transfer.service';
import { data } from '../../../../../mock/transactions.json';
@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.scss']
})
export class TransferComponent implements OnInit {

  constructor(
    private readonly transferService: TransferService
  ) { }

  ngOnInit(): void {
    console.log(data);
    this.transferService.recentTransactions$.next(data);
  }

}

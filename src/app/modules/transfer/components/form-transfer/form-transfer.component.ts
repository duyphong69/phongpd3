import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { TRANSFER_STATE, regexCheckDecimal } from '../../shared/transfer.const';
import { TransferService } from '../../shared/transfer.service';
import { takeUntil, map } from 'rxjs/operators';
import { IRecentTransaction } from '../../shared/transfer.models';
import { getRandomColor } from '../../shared/utils.transfer';
@Component({
  selector: 'app-form-transfer',
  templateUrl: './form-transfer.component.html',
  styleUrls: ['./form-transfer.component.scss']
})
export class FormTransferComponent implements OnInit, OnDestroy {

  errMsg = {
    errToAccount: '',
    errAmount: ''
  };
  transactionBody: IRecentTransaction;
  transferState = TRANSFER_STATE;
  transferForm: FormGroup;
  unsubscribe$ = new Subject();
  isShowCfReset = false;
  isShowCfValidate = false;
  constructor(
    private readonly fb: FormBuilder,
    public transferService: TransferService,
  ) { }

  ngOnInit(): void {
    this.transferService.transferState$.next(TRANSFER_STATE.INPUT);
    this.subscribeTransferState();
    this.initTransferForm();
  }

  initTransferForm(): void {
    this.transferForm = this.fb.group({
      fromAccount: new FormControl({ value: `PhongPD3 $${this.transferService.amount}`, disabled: true }),
      toAccount: new FormControl(null, [Validators.required, Validators.maxLength(100)]),
      amount: new FormControl(null, [
        Validators.required,
        Validators.pattern(regexCheckDecimal)
      ])
    });
  }

  /**
   * @method backToInput
   * change transferState from CONFRIM to INPUT
   */
  backToInput(): void {
    this.transferService.transferState$.next(TRANSFER_STATE.INPUT);
  }

  confirmTransaction(): void {
    const { amount } = this.transferForm.value;
    this.transactionBody = {
      categoryCode: getRandomColor(),
      dates: {
        valueDate: new Date().getTime()
      },
      merchant: {
        accountNumber: 'SI64397745065188826',
        name: 'Backbase',
      },
      transaction: {
        amountCurrency: {
          amount,
          currencyCode: 'EUR',
        },
        creditDebitIndicator: 'DBIT',
        type: 'Online Transfer',
      },
    };
    const convertAmount = Math.round(amount * 100) / 100;
    console.log(convertAmount);
    this.transferService.amount = Math.round(((this.transferService.amount - convertAmount) * 100) / 100);
    console.log(this.transferService.amount);
    this.transferService.recentTransactions$.next(
      this.transferService.recentTransactions$.getValue().concat([this.transactionBody])
    );
    this.transferService.transferState$.next(TRANSFER_STATE.SUCCESSFULLY);
  }

  finishTransaction(): void {
    this.resetForm();
    this.transferService.transferState$.next(TRANSFER_STATE.INPUT);
  }

  handleReset(): void {
    this.isShowCfReset = false;
    this.resetForm();
  }

  /**
   * @method handleClickImgReset
   */
  handleClickImgReset(): void {
    const { toAccount, amount } = this.transferForm.value;
    if (toAccount || amount) {
      this.isShowCfReset = true;
    }
  }
  resetForm(): void {
    const amount = this.transferService.amount < 0 ? `-$${this.transferService.amount * -1}` : `$${this.transferService.amount}`;
    this.transferForm.setValue({
      fromAccount: `PhongPD3 ${amount}`,
      toAccount: null,
      amount: null
    });
  }

  handleTransfer(): void {
    if (this.transferForm.valid) {
      this.errMsg = {
        errToAccount: '',
        errAmount: ''
      };
      if ((this.transferService.amount - Math.round(this.transferForm.value.amount * 100) / 100) < -500) {
        this.isShowCfValidate = true;
      } else {
        this.transferService.transferState$.next(TRANSFER_STATE.CONFIRM);
      }
    } else {
      this.validateTransferForm();
    }
  }

  validateTransferForm(): void {
    this.errMsg = {
      errToAccount: '',
      errAmount: ''
    };
    if (this.transferForm.controls.toAccount.invalid) {
      this.errMsg.errToAccount = 'transfer.error_to_account';
    }
    if (this.transferForm.controls.amount.invalid) {
      this.errMsg.errAmount = 'transfer.error_amount';
    }
  }

  subscribeTransferState(): void {
    this.transferService.transferState$.pipe(takeUntil(this.unsubscribe$)).subscribe((state: TRANSFER_STATE) => {
      switch (state) {
        case TRANSFER_STATE.INPUT:
          break;
        case TRANSFER_STATE.CONFIRM:
          break;
        case TRANSFER_STATE.SUCCESSFULLY:
          break;
        case TRANSFER_STATE.FAILED:
        default:
          break;
      }
    });
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }

}

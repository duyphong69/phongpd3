import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ViewContainerRef,
  ComponentFactoryResolver,
  Compiler
} from '@angular/core';
import { TransferService } from '../../shared/transfer.service';
import { IRecentTransaction } from '../../shared/transfer.models';
import { Subject } from 'rxjs';
import { debounceTime, takeUntil, map } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import { findImgPath, getSortStateByKey } from '../../shared/utils.transfer';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { SORT_STATE, MONTH, KEY_SORT } from '../../shared/transfer.const';
@Component({
  selector: 'app-recent-transactions',
  templateUrl: './recent-transactions.component.html',
  styleUrls: ['./recent-transactions.component.scss']
})
export class RecentTransactionsComponent implements OnInit, OnDestroy {

  isShowDetail = false;
  itemDetail: IRecentTransaction;
  unsubscribe$ = new Subject();
  recentTransactions: IRecentTransaction[];
  recentTransactionsRaw: IRecentTransaction[];
  @ViewChild('dynamicRecentTransaction', { read: ViewContainerRef, static: true })
  containerRef: ViewContainerRef;
  searchFrom: FormGroup;
  sortStateConst = SORT_STATE;
  sortState: string;
  constructor(
    private readonly transferService: TransferService,
    private readonly cfr: ComponentFactoryResolver,
    private compiler: Compiler,
    private readonly fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.initFormGroup();
    this.subscribeForm();
    this.transferService.sortState$.next(this.sortStateConst.DATE_DESC);
    this.transferService.sortState$.pipe(takeUntil(this.unsubscribe$)).subscribe(state => {
      this.sortState = state;
    });
    this.transferService.recentTransactions$.pipe(
      // deep copy. consider change solution if many object nester
      map(datas => JSON.parse(JSON.stringify(datas))),
      map(datas => this.handleSortBySortState(datas)),
      takeUntil(this.unsubscribe$)).subscribe(datas => {
        console.log(datas);
        const datePipe = new DatePipe('en-US');
        this.recentTransactions = datas.map(data => {
          const date = datePipe.transform(data.dates.valueDate, 'MM. d');
          const splitdate = date.split('.');
          const dateConvert = MONTH[splitdate[0]];
          const allTextDisplay = date.replace(splitdate[0], dateConvert) + data.merchant.name + data.transaction.type + `-$${data.transaction.amountCurrency.amount}`;
          data.allTextDisplay = allTextDisplay;
          data.imgPath = findImgPath(data);
          return data;
        });
        this.recentTransactionsRaw = this.recentTransactions;
      });
  }

  handleSortBySortState(RecentTransactions: IRecentTransaction[]): IRecentTransaction[] {
    // TODO consider refactor sort
    let result = RecentTransactions;
    console.log(result);
    if (this.sortState.includes('DATE')) {
      let order = 1;
      if (this.sortState === SORT_STATE.DATE_DESC) {
        order = -1;
      }
      result = RecentTransactions.sort((first, second) => {
        if ((new Date(first.dates.valueDate).getTime()) > (new Date(second.dates.valueDate).getTime())) {
          return order;
        } else {
          return order * -1;
        }
      });
    } else if (this.sortState.includes('BENEFICARY')) {
      let order = 1;
      if (this.sortState === SORT_STATE.BENEFICARY_ASC) {
        order = -1;
      }
      result = RecentTransactions.sort((first, second) => {
        if (first.merchant.name > second.merchant.name) {
          return order;
        } else {
          return order * -1;
        }
      });
    } else if (this.sortState.includes('AMOUNT')) {
      let order = 1;
      if (this.sortState === SORT_STATE.AMOUNT_ASC) {
        order = -1;
      }
      result = RecentTransactions.sort((first, second) => {
        if (+first.transaction.amountCurrency.amount > +second.transaction.amountCurrency.amount) {
          return order;
        } else {
          return order * -1;
        }
      });
    }
    return result;
  }

  initFormGroup(): void {
    this.searchFrom = this.fb.group({
      searchControl: new FormControl(null)
    });
  }

  subscribeForm(): void {
    this.searchFrom.valueChanges.pipe(
      debounceTime(300),
      takeUntil(this.unsubscribe$)
    ).subscribe(text => {
      console.log(text);
      if (text.searchControl) {
        this.recentTransactions = this.recentTransactionsRaw.filter(item => item.allTextDisplay.includes(text.searchControl));
      } else {
        this.recentTransactions = JSON.parse(JSON.stringify(this.recentTransactionsRaw));
      }
    }
    );
  }

  handleClearSearch(): void {
    this.searchFrom.controls.searchControl.setValue(null);
  }

  async detailItem(item: IRecentTransaction): Promise<void> {
    this.itemDetail = item;
    this.isShowDetail = true;
    // TODO thinking use dynamic component
    // const { RecentTransactionDetailComponent } = await import('../recent-transaction-detail/recent-transaction-detail.component');
    // const recentTransactions = this.cfr.resolveComponentFactory(RecentTransactionDetailComponent);
    // const componentRef = this.containerRef.createComponent(recentTransactions);
    // componentRef.instance.recentTransaction = item;
  }

  dateSort(): void {
    // let result;
    // if (this.sortState.includes('DATE')) {
    //   result = (this.sortStateConst.DATE_ASC === this.sortState) ? this.sortStateConst.DATE_DESC : this.sortStateConst.DATE_ASC;
    // } else {
    //   result = this.sortState.includes('ASC') ? this.sortStateConst.DATE_ASC : this.sortStateConst.DATE_DESC;
    // }
    // return result;
    const sortState = getSortStateByKey(KEY_SORT.DATE, this.sortState);
    this.transferService.sortState$.next(sortState);
    this.recentTransactions = this.handleSortBySortState(this.recentTransactions);
  }

  beneficarySort(): void {
    const sortState = getSortStateByKey(KEY_SORT.BENEFICARY, this.sortState);
    this.transferService.sortState$.next(sortState);
    this.recentTransactions = this.handleSortBySortState(this.recentTransactions);
  }

  amountSort(): void {
    const sortState = getSortStateByKey(KEY_SORT.AMOUNT, this.sortState);
    this.transferService.sortState$.next(sortState);
    this.recentTransactions = this.handleSortBySortState(this.recentTransactions);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }

}

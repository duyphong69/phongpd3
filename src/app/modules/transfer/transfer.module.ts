import { NgModule } from '@angular/core';
import { TransferComponent } from './pages/transfer/transfer.component';
import { AppRoutingModule } from './transfer-routing.module';
import { SharedModule } from '@shared/modules/shared.module';
import { FormTransferComponent } from './components/form-transfer/form-transfer.component';
import { RecentTransactionsComponent } from './components/recent-transactions/recent-transactions.component';
// import { RecentTransactionDetailComponent } from './components/recent-transaction-detail/recent-transaction-detail.component';
import { BrowserModule } from '@angular/platform-browser';
import { ModalComponent } from '../../shared/components/modal/modal.component';
@NgModule({
  declarations: [
    TransferComponent,
    FormTransferComponent,
    RecentTransactionsComponent,
    ModalComponent
  ],
  imports: [
    AppRoutingModule,
    SharedModule
  ]
})
export class TransferModule { }

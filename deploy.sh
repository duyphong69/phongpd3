#!/bin/sh
aws s3 rm "$1" --recursive --profile peachtreebank_dev
aws s3 sync "$3" "$1" --profile peachtreebank_dev
aws configure set preview.cloudfront true
aws configure set preview.cloudfront true --profile peachtreebank_dev
aws cloudfront --profile peachtreebank_dev create-invalidation --distribution-id "$2" --paths "/*"